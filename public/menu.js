var menu_style = { fill: 'yellow', fontSize: '30px' };
var unselect_style = { fill: '#ffffff', fontSize: '30px' };
var select_one_in_menu = 0;
var Menu = {
    preload:function(){
        game.load.audio('bg','./assets/bgmusic.mp3');
        game.load.image('1','1.jpg');
    },
    create:function(){
        game.add.image(-50,-200,'1');
        var nameLabel = game.add.text(60,80,'Fucking Stairs',{font:'50px Arial',fill:'red'});
        //var startLabel = game.add.text(80,game.world.height-80,'press the "Enter" key to start' ,{font:'25px Arial',fill:'#ffffff'});
        this.getranking();

       /* bg_music = game.add.audio('bg');
        bg_music.play();*/
        if (select_one_in_menu == 0) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 1) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 2) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
        }
        game.input.keyboard.addKey(Phaser.Keyboard.UP).onDown.add(this.previosoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.DOWN).onDown.add(this.nextoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.W).onDown.add(this.previosoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.S).onDown.add(this.nextoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.ENTER).onDown.add(this.start, this);
        
  }  ,
  start:function(){
    
        if(select_one_in_menu==0)
            game.state.start('index');
        else if(select_one_in_menu==1)
            game.state.start('players');
        else if(select_one_in_menu==2)
            game.state.start('score');
  },
   nextoption:function() {
    //usic.play();
    if (select_one_in_menu == 0) {
        select_one_in_menu = 1;
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
    } else if (select_one_in_menu == 1) {
        select_one_in_menu = 2;
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
    } else if (select_one_in_menu == 2) {
        select_one_in_menu = 0;
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
    }
},
 previosoption :function(){
    //selectmusic.play();
    if (select_one_in_menu == 0) {
        select_one_in_menu = 2;
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
    } else if (select_one_in_menu == 1) {
        select_one_in_menu = 0;
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
    } else if (select_one_in_menu == 2) {
        select_one_in_menu = 1;
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
    }
},
 getranking:function() {
    var postsRef = firebase.database().ref('Record');
    var total_post = [];
    var name_post = [];
    var time_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var name = childSnapshot.val().email;
                var bestrecord = childSnapshot.val().data;
                var year = childSnapshot.val().Year;
                var month = childSnapshot.val().Month;
                var day = childSnapshot.val().Day;
                var hours = childSnapshot.val().Hours;
                var minutes = childSnapshot.val().Minutes;
                var seconds = childSnapshot.val().Seconds;
                if (hours < 10)
                    hours = '0' + hours;
                if (minutes < 10)
                    minutes = '0' + minutes;
                if (seconds < 10)
                    seconds = '0' + seconds;
                total_post[total_post.length] = bestrecord;
                name_post[name_post.length] = name;
                time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
                first_count += 1;
            });

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var name = data.val().email;
                    var bestrecord = data.val().data;
                    var year = data.val().Year;
                    var month = data.val().Month;
                    var day = data.val().Day;
                    var hours = data.val().Hours;
                    var minutes = data.val().Minutes;
                    var seconds = data.val().Seconds;
                    if (hours < 10)
                        hours = '0' + hours;
                    if (minutes < 10)
                        minutes = '0' + minutes;
                    if (seconds < 10)
                        seconds = '0' + seconds;
                    total_post[total_post.length] = bestrecord;
                    name_post[name_post.length] = name;
                    time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
                }
            });
            for (var i = total_post.length - 1; i > 0; i--) {
                for (var j = 0; j < i; j++) {
                    if (total_post[j] < total_post[j + 1]) {
                        var tmp, tmp_name, tmp_time;

                        tmp = total_post[j];
                        total_post[j] = total_post[j + 1];
                        total_post[j + 1] = tmp;

                        tmp_name = name_post[j];
                        name_post[j] = name_post[j + 1];
                        name_post[j + 1] = tmp_name;

                        tmp_time = time_post[j];
                        time_post[j] = time_post[j + 1];
                        time_post[j + 1] = tmp_time;
                    }
                }
            }
            firstplace_name = name_post[0];
            firstplace_record = total_post[0];
            firstplace_time = time_post[0];
            secondplace_name = name_post[1];
            secondplace_record = total_post[1];
            secondplace_time = time_post[1];
            thirdplace_name = name_post[2];
            thirdplace_record = total_post[2];
            thirdplace_time = time_post[2];
            number_of_users = total_post.length;
            console.log('lenght: ' + total_post.length);
            console.log('1.' + firstplace_name + ':' + firstplace_record + '->' + firstplace_time);
            console.log('2.' + secondplace_name + ':' + secondplace_record + '->' + secondplace_time);
            console.log('3.' + thirdplace_name + ':' + thirdplace_record + '->' + thirdplace_time);
        })
        .catch(e => console.log(e.message));
}
};


